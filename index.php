<?php
include_once('include/layout/header.php');
?>
    <!--loader-->
    <!--    <div class='preloader text-center'>-->
    <!--        <div class='louder wow flash' data-wow-duration="3s" data-wow-iteration="100"><img alt="logo" class=""  src="--><?php //echo $URL; ?><!--assets/img/icon.png">-->
    <!--           </div>-->
    <!--    </div>-->
    <!--end loader-->
    <body>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M67PZ67"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section class="intro" id="intro">
        <div class="container   ">
            <div class="align-items-center row ">
                <div class="col-12   px-5 mt-20">
                    <div class="intro-text  text-white m-auto">
                        <h1 class="yellow ">Chauffeur Service Dubai</h1>
                        <p class="">Live your life luxuriously and elegantly! Take a graceful drive with a professional
                            driver
                            from
                            our <b>Chauffeur Services in Dubai</b> for comfort and ease.
                        </p>

                    </div>
                </div>
                <div class="col-12 px-5 pb-4  text-center">
                    <div class="mt-65">

                        <form class="text-left text-white" id="bookForm1" data-parsley-validate>
                            <div class="bg-dark-opacity px-5 py-3 ">
                                <div class="form-row b-white-border ">
                                    <div class="form-group col-12 col-md-4 r-gray-border">
                                        <label class="label-form" for="name1">Name <sup>*</sup></label>
                                        <input type="text" class="form-control opacity-form" id="name1"
                                               placeholder="Your Name" required="">
                                    </div>
                                    <div class="form-group col-12 col-md-4 r-gray-border">
                                        <label class="label-form w-100" for="phone1">Phone <sup>*</sup></label>
                                        <input type="tel" class="form-control opacity-form phone" id="phone1"
                                               placeholder="Your Number" required=""
                                               data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$"
                                        >
                                    </div>
                                    <div class="form-group col-12 col-md-4">
                                        <label class="label-form" for="car1">Choose your favourite car
                                            <sup>*</sup></label>
                                        <select id="car1" class="form-control opacity-form" required="">
                                            <option selected disabled>-</option>
                                            <option>Rolls Royce Cullinan</option>
                                            <option>Bentley Flying Spur</option>
                                            <option>Bentley Bentayga</option>
                                            <option>Rolls Royce Wraith Black Badge</option>
                                            <option>Mercedes S500</option>
                                            <option>Rolls Royce Ghost</option>
                                            <option>Tesla Model X</option>
                                            <option>Cadillac Escalade 2021</option>
                                            <option>Audi Q8</option>
                                            <option>Mercedes S CLass</option>
                                            <option>Porsche Cayenne</option>
                                            <option>BMW 740Li</option>
                                            <option>Toyota Land Cruiser</option>
                                            <option>Cadillac Escalade</option>
                                            <option>Lexus LX570</option>
                                            <option>Audi Q7</option>
                                            <option>GMC Yukon Denali</option>
                                            <option>BMW 520i</option>
                                            <option>Nissan Patrol</option>
                                            <option>Audi A6</option>
                                            <option>Range Rover Vogue</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label class="label-form" for="message1">Your Message</label>
                                    <textarea id="message1" class="form-control opacity-form" placeholder="Message"
                                              rows="1"></textarea>
                                </div>
                            </div>
                            <div class="text-center w-100">
                                <div class="contact-error alert alert-danger mt-3">Try Again Later</div>
                                <div class="contact-success alert alert-success mt-3">Submitted. We will get back to you
                                    shortly.
                                </div>
                                <button type="submit" class="btn btn-warning mt-3">Book Now</button>
                            </div>
                        </form>


                    </div>

                </div>


            </div>
        </div>
    </section>
    <section class="rental my-5" id="rental">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        Why Our Luxury Chauffeur Service?

                    </h2>
                    <p class="divider m-auto"></p>
                    <p class="mt-3 gray fs-18">
                        Let go of the wheel and have your troubles fade away as a Chauffeur takes over your drive.
                        Whether you're going to a business meeting, a late dinner party, or picking up a friend, we can
                        make it easy for you. Have our professional take you where you want to go in the dream car of
                        your choice.
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <h2 class="mb-0">Your Thrilling Rental</h2>
                    <p class="gray d-block">Which car speaks to you?</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Rolls Royce Cullinan" class="bookModal"><img class="card-img-top" src="assets/img/Rolls-Royce-Cullinan-2.webp" alt="Rolls Royce Cullinan">
                        </a><h3 class="card-title">Rolls Royce Cullinan</h3>
                        <div class="custom-card-body">
                            <div class="card-pricee"><span class="price">7500 </span><span class="aed">AED</span></div> 
                            <div class="card-apply">*T&C Apply</div>
                            <div class="card-btns">
                                <div class="row">
                                    <div class="col1">
                                        <button class="btn btn-warning card-btn whats-btn">
                                        <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Rolls Royce Cullinan'.">    
                                        <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                        </a>
                                        </button>
                                    </div>

                                    <div class="col2">
                                        <button class="btn btn-warning card-btn bookModal" 
                                        data-car="Rolls Royce Cullinan"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                        </button> 
                                    </div>

                                    <div class="col1">
                                        <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Bentley Flying Spur" class="bookModal"><img class="card-img-top" src="assets/img/Bentley-Flying-Spur.webp" alt="Bentley Flying Spur">
                        </a><h3 class="card-title">Bentley Flying Spur</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">4500 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Bentley Flying Spur'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Bentley Flying Spur"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Bentley Bentayga" class="bookModal"><img class="card-img-top" src="assets/img/Bentley-Bentayga_1.webp" alt="Bentley Bentayga">
                        </a><h3 class="card-title">Bentley Bentayga</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">4000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Bentley Bentayga'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Bentley Bentayga"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Rolls Royce Wraith Black Badge" class="bookModal"><img class="card-img-top" src="assets/img/ROLLS-ROYCE-WRAITH-BLACK-BADGE.webp"
                             alt="Rolls Royce Wraith Black Badge">
                        </a><h3 class="card-title c-t-over">Rolls Royce Wraith Black Badge</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">4000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Rolls Royce Wraith Black Badge'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Rolls Royce Wraith Black Badge"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Mercedes S500" class="bookModal"><img class="card-img-top" src="assets/img/Mercedes-S500.webp"
                             alt="Mercedes S500">
                        </a><h3 class="card-title">Mercedes S500</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">3000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Mercedes S500'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Mercedes S500"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Rolls Royce Ghost" class="bookModal"><img class="card-img-top" src="assets/img/Rolls-Royce-Ghost-1.webp" alt="Rolls Royce Ghost">
                        </a><h3 class="card-title">Rolls Royce Ghost</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">4000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Rolls Royce Ghost'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Rolls Royce Ghost"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Tesla Model X" class="bookModal"><img class="card-img-top" src="assets/img/Tesla-Model-X (1).webp" alt="Tesla Model X">
                        </a><h3 class="card-title">Tesla Model X</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2300 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Tesla Model X'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Tesla Model X"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Cadillac Escalade 2021" class="bookModal"><img class="card-img-top" src="assets/img/Cadillac-Escalade-front-side.jpg" alt="Cadillac Escalade 2021">
                        </a><h3 class="card-title">Cadillac Escalade 2021</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2800 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Cadillac Escalade 2021'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Cadillac Escalade 2021"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Audi Q8" class="bookModal"><img class="card-img-top" src="assets/img/Audi-Q8.webp" alt="Audi Q8">
                        </a><h3 class="card-title">Audi Q8</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Audi Q8'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Audi Q8"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Mercedes S CLass" class="bookModal"><img class="card-img-top" src="assets/img/Mercedes-S560.webp" alt="Mercedes S560">
                        </a><h3 class="card-title">Mercedes S CLass</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Mercedes S CLass'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Mercedes S CLass"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Porsche Cayenne" class="bookModal"><img class="card-img-top" src="assets/img/Porsche-Cayenne_2.webp" alt="Porsche Cayenne">
                        </a><h3 class="card-title">Porsche Cayenne</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2300 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Porsche Cayenne'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Porsche Cayenne"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Range Rover Vogue" class="bookModal"><img class="card-img-top" src="assets/img/Range-Rover-Vogue.webp" alt="Range Rover Vogue">
                        </a><h3 class="card-title">Range Rover Vogue</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Range Rover Vogue'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Range Rover Vogue"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="BMW 740Li" class="bookModal"><img class="card-img-top" src="assets/img/BMW-740Li.webp" alt="Bmw 740Li">
                        </a><h3 class="card-title">BMW 740Li</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'BMW 740Li'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="BMW 740Li"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Toyota Land Cruiser" class="bookModal"><img class="card-img-top" src="assets/img/Toyota Land Cruiser.jpg" alt="Toyota Land Cruiser">
                        </a><h3 class="card-title">Toyota Land Cruiser</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1800 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Toyota Land Cruiser'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Toyota Land Cruiser"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Cadillac Escalade" class="bookModal"><img class="card-img-top" src="assets/img/Cadillac-Escalade.webp" alt="Cadillac Escalade">
                        </a><h3 class="card-title">Cadillac Escalade</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1800 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Cadillac Escalade'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Cadillac Escalade"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Lexus LX570" class="bookModal"><img class="card-img-top" src="assets/img/Lexus-Lx570.webp" alt="Lexus LX570">
                        </a><h3 class="card-title">Lexus LX570</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">2000 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Lexus LX570'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Lexus LX570"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Audi Q7" class="bookModal"><img class="card-img-top" src="assets/img/AUDI-Q7.webp" alt="Audi Q7">
                        </a><h3 class="card-title">Audi Q7</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1500 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Audi Q7'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Audi Q7"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="GMC Yukon Denali" class="bookModal"><img class="card-img-top" src="assets/img/gmc.webp" alt="GMC Yukon Denali">
                        </a><h3 class="card-title">GMC Yukon Denali</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1800 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'GMC Yukon Denali'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="GMC Yukon Denali"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="BMW 520i" class="bookModal"><img class="card-img-top" src="assets/img/BMW-520i.webp" alt="BMW 520i">
                        </a><h3 class="card-title">BMW 520i</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1500 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'BMW 520i'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="BMW 520i"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Nissan Patrol" class="bookModal"><img class="card-img-top" src="assets/img/Nissan-Patrol.webp" alt="Nissan Patrol">
                        </a><h3 class="card-title">Nissan Patrol</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1500 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Nissan Patrol'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Nissan Patrol"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 my-4">
                    <div class="card text-center wow fadeInUp custom-card">
                        <a data-car="Audi A6" class="bookModal"><img class="card-img-top" src="assets/img/Audi-A6.webp" alt="Audi A6">
                        </a><h3 class="card-title">Audi A6</h3>
                        <div class="custom-card-body">
                        <div class="card-pricee"><span class="price">1200 </span><span class="aed">AED</span></div> 
                        <div class="card-apply">*T&C Apply</div>
                        <div class="card-btns">
                            <div class="row">
                                <div class="col1">
                                    <button class="btn btn-warning card-btn whats-btn">
                                    <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Audi A6'.">    
                                    <span><i class="fa fa-whatsapp c-icon"></i> WhatsApp</span>
                                    </a>
                                    </button>
                                </div>

                                <div class="col2">
                                    <button class="btn btn-warning card-btn bookModal" 
                                    data-car="Audi A6"><span><i class="fa fa-calendar-check-o c-icon"></i> Inquire Now</span>
                                    </button> 
                                </div>

                                <div class="col1">
                                    <button class="btn btn-warning card-btn call-btn" ><a href="tel:+971553451555"><span><i class="fa fa-phone c-icon"></i> Call Us</a></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>


        </div>
        </div>
    </section>
    <section class="service my-5" id="services">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        A Pleasant & Delightful Experience
                    </h2>
                    <p class="divider m-auto"></p>
                    <p class="mt-3 gray fs-18">
                        Let our <b>Chauffeur services</b> give you a graceful drive around the city of Dubai in a
                        luxurious
                        rental. No steering, no parking, and no worrying about the road. Just smooth cursing in the
                        comfort of the backseat.
                    </p>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-12 col-md-6 col-lg-3 text-center wow fadeInUp">
                    <img src="assets/img/icon7.png" alt="Competitive Prices" class="icon-65 my-2">
                    <h3 class="gray">Competitive Prices</h3>
                    <p class="fs-12">Rent your favorite vehicle at promising prices and mind-blowing offers with
                        long-term rental deals.</p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 text-center wow fadeInUp">
                    <img src="assets/img/icon8.png" alt="Top-Notch Professionals" class="icon-65 my-2">
                    <h3 class="gray">Top-Notch Professionals</h3>
                    <p class="fs-12">Put your mind at ease as our well-trained drivers take you on a marvelous trip in
                        your luxurious dream vehicle</p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 text-center wow fadeInUp">
                    <img src="assets/img/icon9.png" alt="Vast Fleet" class="icon-65 my-2">
                    <h3 class="gray">Vast Fleet</h3>
                    <p class="fs-12">No matter what kind of budget or taste for cars you have, you can find it among our
                        wide selection of glorious cars.</p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 text-center wow fadeInUp">
                    <img src="assets/img/icon10.png" alt="Quick Response" class="icon-65 my-2">
                    <h3 class="gray">Quick Response</h3>
                    <p class="fs-12">Our 24/7 customer service hotline will attend to all your needs and requirements as
                        quickly as possible.</p>
                </div>
            </div>

        </div>
    </section>
    <!--    for web-->
    <section class="overflow-hidden d-none d-md-block">
        <div class="container p-0 p-md-3">
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 p-md-3 p-0">
                    <img src="assets/img/chauffeur-service-with-driver.webp " alt="Chauffeur Service with Driver"
                         class="w-100 zoom">
                </div>
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="  pt-4 pl-4   bg-blue">
                        <div class="border-top-left-3 pt-1 pl-1">
                            <div class="border-top-left pt-1 pl-1">
                                <h2 class="text-white px-3 pt-3 wow fadeInLeft">Chauffeur Service with Driver</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-3 pb-4 wow fadeInLeft">Renting your very own
                                    luxury car for a smooth, mellow drive can be enhanced with our <b>Chauffeur services
                                        in Dubai</b>. Leave it to our professional drivers to take you to your desired
                                    destination with ease and on schedule. Imagine arriving at your business meeting
                                    both ahead of schedule and in a marvelous-looking vehicle. You don't have to worry
                                    about traffic, the road, or where to park your car. All of your driving concerns are
                                    handled by highly trained drivers and multilingual professionals. It's this kind of
                                    luxury service that sets you apart and has you living up to the expectations of this
                                    modern city.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="pt-4 pr-4  bg-orange">
                        <div class="border-top-right-3 pt-1 pr-1">
                            <div class="border-top-right pt-1 pr-1">
                                <h2 class="text-white px-4 pt-3 wow fadeInRight ">Limousine Service</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-4 pb-4 wow fadeInRight ">Take your driving
                                    experience to the next level and have elite drivers pick you up and drop you off
                                    with class. A limousine service will embrace your inner chic personality and is a
                                    thrilling experience at the same time. Perfect for going to parties and late-night
                                    fun in the city. Dress to impress is no longer the issue these days because we have
                                    luxury cars to make that impression. Take your partner, date, or group of friends on
                                    a classy drive and make this night memorable for everyone in a luxury limousine.
                                    Don't forget to pop your head out of the limo's sunroof. That would be a good story
                                    to tell.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 p-md-3 p-0">
                    <img src="assets/img/limousin-service.webp" alt="Limousin Service" class="w-100 zoom">
                </div>
            </div>
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 p-md-3 p-0">
                    <img src="assets/img/airport-service.jpg" alt="Airport Service" class="w-100 zoom">
                </div>
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="  pt-4 pl-4  bg-blue">
                        <div class="border-top-left-3 pt-1 pl-1">
                            <div class="border-top-left pt-1 pl-1">
                                <h2 class="text-white px-3 pt-3 wow fadeInLeft">Airport Car Rental</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-3 pb-4 wow fadeInLeft">Don't waste your time and
                                    money on cabs and public transportation. It's all about the luxury experience when
                                    living or visiting Dubai. That's why you should consider renting an excellent car
                                    with an extraordinary driver while you stay here'no more waiting for a cab or your
                                    relative to come to pick you up from the airport. We offer you an elegant and
                                    relaxing Airport car rental pickup and drop-off whenever you need it. Our drivers
                                    are punctual, bilingual, and have excellent driving experience. So, why don't you
                                    lay your head back in the backseat of your favorite car as you're driven across the
                                    great city of Dubai?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="pt-4 pr-4  bg-orange">
                        <div class="border-top-right-3 pt-1 pr-1">
                            <div class="border-top-right pt-1 pr-1">
                                <h2 class="text-white px-4 pt-3 wow fadeInRight ">Monthly Car Rental</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-4 pb-4 wow fadeInRight ">We strive to provide
                                    great prices and high-quality cars. We stop at nothing to attain top-notch vehicles
                                    for rent and highly-trained drivers for your chauffeur requirements. You can find
                                    the car of your dreams from our vast fleet with the perfect driver as well. Great
                                    offers and deals can also be found at our company, for we provide competitive prices
                                    for marvelous vehicles and long-term rentals. Whether you want your <b>Chauffeur
                                        services in Dubai</b> for a day, week, month, or even a year, we got you
                                    covered. All you have to do is find the best car that suits your requirements for a
                                    marvelous drive in the city.

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 p-md-3 py-0 ">
                    <img src="assets/img/monthly-service.jpg" alt="Monthly Service" class="w-100 zoom">
                </div>
            </div>

        </div>
    </section>
    <!--    end for web-->
    <!--    for mobile-->
    <section class="overflow-hidden d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-12 by-3 my-3">
                    <div class="service-blue-bg wow fadeInUp">
                        <h2>
                            Chauffeur Service with Driver
                        </h2>
                        <p class="three-line see-more-1" data-type="close">
                            Renting your very own luxury car for a smooth, mellow drive can be enhanced with our
                            Chauffeur services in Dubai. Leave it to our professional drivers to take you to your
                            desired destination with ease and on schedule. Imagine arriving at your business meeting
                            both ahead of schedule and in a marvelous-looking vehicle. You don't have to worry about
                            traffic, the road, or where to park your car. All of your driving concerns are handled by
                            highly trained drivers and multilingual professionals. It's this kind of luxury service that
                            sets you apart and has you living up to the expectations of this modern city.
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="1">Read More</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 by-3 my-3">
                    <div class="service-orange-bg wow fadeInUp">
                        <h2>
                            Limousine Service
                        </h2>
                        <p class="three-line see-more-2" data-type="close">
                            Take your driving experience to the next level and have elite drivers pick you up and drop
                            you off with class. A limousine service will embrace your inner chic personality and is a
                            thrilling experience at the same time. Perfect for going to parties and late-night fun in
                            the city. Dress to impress is no longer the issue these days because we have luxury cars to
                            make that impression. Take your partner, date, or group of friends on a classy drive and
                            make this night memorable for everyone in a luxury limousine. Don't forget to pop your head
                            out of the limo's sunroof. That would be a good story to tell.
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="2">Read More</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 by-3 my-3">
                    <div class="service-blue-bg wow fadeInUp">
                        <h2>
                            Airport Car Rental
                        </h2>
                        <p class="three-line see-more-3" data-type="close">
                            Don't waste your time and money on cabs and public transportation. It's all about the luxury
                            experience when living or visiting Dubai. That's why you should consider renting an
                            excellent car with an extraordinary driver while you stay here'no more waiting for a cab or
                            your relative to come to pick you up from the airport. We offer you an elegant and relaxing
                            Airport car rental pickup and drop-off whenever you need it. Our drivers are punctual,
                            bilingual, and have excellent driving experience. So, why don't you lay your head back in
                            the backseat of your favorite car as you're driven across the great city of Dubai?
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="3">Read More</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 by-3 my-3">
                    <div class="service-orange-bg wow fadeInUp">
                        <h2>
                            Monthly Car Rental

                        </h2>
                        <p class="three-line see-more-4" data-type="close">
                            We strive to provide great prices and high-quality cars. We stop at nothing to attain
                            top-notch vehicles for rent and highly-trained drivers for your chauffeur requirements. You
                            can find the car of your dreams from our vast fleet with the perfect driver as well. Great
                            offers and deals can also be found at our company, for we provide competitive prices for
                            marvelous vehicles and long-term rentals. Whether you want your Chauffeur services in Dubai
                            for a day, week, month, or even a year, we got you covered. All you have to do is find the
                            best car that suits your requirements for a marvelous drive in the city.
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="4">Read More</span>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>
    <!--    end for mobile-->
    <section class="my-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 p-0">
                    <img src="assets/img/music1.webp" alt="music Video Car" class="w-100 car-bar-img">
                    <div class="car-bar-text">
                        <h2 class="shadow-text">Music Video Car</h2>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 p-0">
                    <img src="assets/img/wedding.webp" alt="Wedding Services" class="w-100 car-bar-img">
                    <div class="car-bar-text">
                        <h2 class="shadow-text">Wedding Services</h2>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4 p-0">
                    <img src="assets/img/events-1.webp" alt="Event Services" class="w-100 car-bar-img">
                    <div class="car-bar-text">
                        <h2 class="shadow-text">Event Services</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-5" id="scrollable-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gray scrollable-text">
                        <div class="text-center"><p class="fs-40" style="font-weight: bold">Innovative Brands &
                                Models</p></div>
                        <h2>Audi Rental Dubai</h2>
                        Innovation, elegance, and a powerful engine. If you're the kind of person looking for these
                        kinds of specifications, then the Audi is your best friend. It's also the perfect car to pick
                        for your <b>Chauffeur services in Dubai</b>. The classiness and breathtaking design of this
                        exotic brand will leave you mesmerized with one look. The interior and exterior are both
                        heavenly and provide excellent comfort and ease to the driver and their companion. That's why
                        you can rely on a modern vehicle like this to be driven around in.
                        <br>
                        Of course, the specification and features for each model vary depending on the engine. Like the
                        Audi Q8 and Audi Q7, the models we have bring forth massive power and speed to the streets of
                        Dubai. You can expect horsepower that reaches about 335 hp and exceptional torque power for easy
                        handling. With this vehicle brand, it's all about strength and driving with style.
                        <br>
                        <h2>Bentley Rental Dubai</h2>
                        Now, when we mention the word elegance, we refer to our very own collection of Bentley rentals
                        in Dubai. These iconic models are designed to bring comfort and style to each person driving or
                        riding in them. You can add that pinch of class to your style with a car like this, especially
                        if you want to live up to the luxury life of Dubai. Furthermore, there's nothing more exciting
                        than to be driven in a vehicle like these. Relaxing in the back seat as you glance at the great
                        modern city and skyscrapers around you is a true delight.
                        <br>
                        Like the Bentley Bantayan and Flying Spur, some of the models we offer don't just look good.
                        These cars also have a surprising punch in them. You can get great engine power that reaches 626
                        hp and substantial torque strength as well. We say there's more than meets the eye with the
                        suspension and transmission on this superb vehicle.
                        <br>
                        <h2>BMW Rental Dubai</h2>
                        A vehicle that represents innovative and modern design with a spectacular performance. Our BMW
                        rental in Dubai is the perfect car to cruise in the streets and attend to all your driving
                        needs. It has a simple look, yet it carries a lot more under the hood. Most people choose this
                        brand because of its smooth and high-quality leather seats. It's very comforting, especially
                        when taken on long drives. Moving around in the city is also pretty neat and easy, thanks to its
                        modern cruise control and fantastic suspension. <br>
                        <br>
                        This iconic brand is also a top pick when it comes to requesting <b>Chauffeur services in
                            Dubai</b>. The luxury taste and stylish presence it has been precisely what high standard
                        living looks like. If you're visiting Dubai for the first time or already a resident, then a
                        modern experience is a must! What better way to live that experience than in this exotic
                        vehicle?
                        <h2>Cadillac Escalade Rental</h2>
                        Living in Dubai is more than just modern age technology and long skyscrapers. It's all about the
                        luxury and elegance you can experience with just one vehicle. We say, to accomplish that
                        milestone, all you need is a good old Cadillac Escalade rental. Driving around in such a massive
                        beast of a car will help you dominate the roads quickly. The best part of it all is that these
                        SUV luxury cars can benefit the entire family if you have one. Plus, our <b>Chauffeur services
                            in Dubai</b> would make seating in this superb vehicle even more astonishing.
                        <br>
                        Specification and features are what set each vehicle apart. Of course, this car can take on any
                        tough road or bumpy hills smoothly, thanks to its massive body and strong engine. With enormous
                        horsepower and a four-wheel-drive option, UAE's roads are in the bags. Dominate and become king
                        of the streets in a monster vehicle like the Cadillac.
                        <h2>GMC Yukon Denali Rental</h2>
                        The more, the merrier is what the GMC Yukon Denali represents. It is the best way to put it when
                        describing this massive vehicle. Nothing stands in the form of actual American muscle,
                        especially when you're facing the scorching deserts of the UAE. You could use a nice, classy,
                        and powerful car in Dubai to cruise the streets and take on bumpy roads. Best of all, the
                        legroom and storage space this brand offers are simply remarkable. You can take the entire
                        family or your gang on long road trips without worrying about extra luggage.
                        <br>
                        With the specifications and features of this SUV, it makes things almost too good to be true.
                        Expect great performance and a high-power engine that produces nearly 420 HP. Moreover, the
                        torque and suspension give the driver the easy steering experience every person should have.
                        Enjoy a luxurious drive with an elegant vehicle like this. <br>
                        <h2>Lexus Rental Dubai</h2>
                        Originated from the word luxury and maintaining its presence as an elegant vehicle in its class.
                        Our Lexus rental in Dubai is some of the best models you could get your hands on. from
                        extraordinary interior to breathtaking exterior design; these cleverly designed SUVs are your
                        gateways to a better and more relaxing life. Even if they catch you as a muscle and strong car,
                        they still have a lot of elegance and class to add to your style and presence. That's why we
                        suggest renting a brand like this because it's how luxury was meant to be experienced.
                        <br>
                        With extra seats in the back and the usual seating system like every SUV, this car was meant for
                        long road trips and vacations. You can take your family or friends on crossroad trips or just
                        hang out under the Dubai nighttime sky. You could always rely on this SUV to attend to all your
                        driving needs and personal requirements.
                        <h2>Mercedes Rental Dubai</h2>
                        Don't cruise. Just glide and the concrete roads of the UAE. If this sounds impossible, you
                        haven't tried our Mercedes rental's thrilling and smooth driving in Dubai. This car provides
                        easy movement and steering on any roads of this great country. You can get to your desired
                        destination at your own pace without being bothered by bumpy roads or the sound of your engine.
                        A calm drive depends on the vehicle you have at hand. Talent and skill have nothing to do with
                        the performance and smooth sailing of your on-road trips.
                        <br>
                        This car's features and details bring forth to say it all, especially models like the Mercedes
                        E300 and S500. A classy automobile like this adds that edge you need in your life from a solid
                        engine for magnificent features. Most importantly, it's very well-suited for our <b>Chauffeur
                            services in Dubai</b> if you need a classy lift.
                        <h2>Nissan Patrol Rental Dubai</h2>
                        Sometimes our budget gets in the way of our needs and requirements. Not to mention if you're
                        spending your income on transportation and private cabs. Fear not, because, with a Nissan Patrol
                        rental in Dubai, your wallet is both safe and happy. You can rent this car to go wherever you
                        want and with a very good deal. We offer great prices on long-term rentals that vary from months
                        to years. You can also book this vehicle for your private Chauffeur services. That way you can
                        be economical on your gas spending and don't have to worry about parking or traffic.
                        <br>
                        This car brand is known for its economical fuel system, which allows you to save a few bucks on
                        gas. Besides excellent fuel economy, it also provides substantial legroom and storage space to
                        fit the entire family and pet (if you have one). Not to mention the intelligent cruise control
                        that turns your long rides into comfortable ones. Time flies fast in this SUV!
                        <h2>Porsche Rental Dubai</h2>
                        Become more modern and up-to-date with the right vehicle for your classy presence. Start getting
                        used to the extra attention and those celebrity vibes from our Porsche rental in Dubai. An
                        elegant and modern specimen that generates a great first impression is planning to dress to
                        impress. Party hard and live life to the fullest in a high-standard car that represents
                        innovation and new-age design. The most iconic benefit from this experience is renting this car
                        with a driver because nothing beats a stress-relieving backseat drive in one of these beauties.
                        <br>
                        What you need to expect from Porsche is nothing less than great performance and a monster of an
                        engine. Don't let looks deceive you because both a graceful look and a strong performance are
                        found in this exotic vehicle. From substantial horsepower to clever equipped technology, your
                        life has just jumped up a level. Get used to it!
                        <h2>Range Rover Rental Dubai</h2>
                        The iconic and popular Range Rover is most people's dream car to drive in Dubai. It's also one
                        of the best cars to rent for our Chauffeur services because of its comfortable, high-quality
                        leather seats and smooth cruising system. It's also a great car for the entire family, for it
                        can fit your partner, kids, and a few extra bags in the back trunk. This vehicle should be your
                        go-to choice if you're planning on a faraway trip across the UAE or just a drive around in the
                        great city of Dubai.
                        <br>
                        It's all about the features and specifications when choosing the right car for your
                        requirements. That's why we provide only the best for you. Take the Range Rover Vogue, for
                        instance. It has the surprisingly outstanding horsepower, a great suspension, and remarkable
                        transmission to top it all off. Best of all, it offers vast legroom and cargo space to make your
                        drives easy and comforting.
                        <h2>Rolls Royce Rental Dubai</h2>
                        Taking you to new heights and dominating the automobile market is Rolls Royce. This remarkable
                        car brand has just given a new name to a chic and elegant style. Of course, This brand has more
                        to say to its drivers than just chic style and great outer body design. The interior also has
                        many outstanding features that make sitting in this vehicle a dream come true. From high-quality
                        seat fabrics to outstanding legroom, you might sometimes wish you could live in this magical
                        car.
                        <br>
                        Although living isn't possible in this vehicle, there are other amazing things it can offer.
                        Like the Rolls Royce Cullinan and Ghost, some of our models come with state-of-the-art features
                        and specs. You can find all you need from strength to easy steering in any car model from this
                        class. You might also want to rent this heavenly design machine with a professional driver to
                        get the utmost fancy experience.
                        <h2>Tesla Rental Dubai</h2>
                        Spread the modern smell and new-age look with the Tesla car brand. Everything that this world
                        needs to become stylish and eco-friendly is in a car that provides it all. Get the latest
                        vehicle technology from cruise control to GPS systems that set you apart from the outer world.
                        Relax as you drive your worries away in the comforting interior with some of the utmost quality
                        material used in its seating system. Wait! It doesn't end there because, with such a humble look
                        and exterior design, this car still has a few surprises up its sleeve.
                        <br>
                        Open up the hood of this car and feist your eyes on what technology has to offer. The engine can
                        burst with exquisite power and exceptional transmission performance. You can also rely on this
                        vehicle for long drives thanks to its front and rear motors with a 4-wheel drive option. Plus,
                        it has that extra space in the back to either fit a couple more passengers or extra bags for
                        your trip.
                        <h2>Limousine Rental Dubai</h2>
                        You can arrive at the party with just your date in a genuine luxury vehicle, or you can take
                        things to the next level and rent a limousine with a driver in Dubai. Nothing says classy than
                        an extended elegant limo parking in front of the door. Better yet, what's classier is someone
                        opening the door for you as you step out of your luxurious limo with your date. Now, if that
                        doesn't grab the attention of everyone around you, we don't know what will.
                        <br>
                        Enjoy a life filled with grace and finesse with our <b>chauffeur services in Dubai</b>. If
                        you're planning on having a memorable night with a few friends or your date, then a limousine is
                        the way to ride. Let our professional drivers take you on a journey of a lifetime while you
                        stick your head out of the sunroof of your long divine vehicle. Your sensational experience is
                        our responsibility to provide.
                        <h2>Professional Chauffeur Service Across Dubai</h2>
                        If you need to clear your mind from all the troubles you have and focus on the real matters at
                        hand, then hire a driver. Sometimes it's easier to have someone handle your driving concerns
                        while you sit back, clear your mind, and think about your next step. Our professional <b>Chauffeur
                            services</b> will do just that for you. The drivers we pick are well trained and bilingual
                        to make airports and other tourist pick-ups easier for clients. Furthermore, they know the
                        routes and roads of this city more than anyone! Thus, you can count on them to get to your
                        destination on time.
                        <br>
                        Dubai is a busy city. There are a lot of things that might trouble or overwhelm you, which could
                        affect your productivity. One of the main reasons for that is traffic and transportation. So,
                        why worry so much when you can count on us?
                        <h2>Affordable Chauffeur Service in Dubai</h2>
                        Luxury is meant to be enjoyed by everyone, and that's why we work on providing you with great
                        prices and offers. You can rent a professional driver with a car of your choice for a day,
                        month, or year. Of course, with longer rentals, you'll get the chance to experience better
                        luxury and at a great price. Maintaining our high-quality services with top-notch cars is our
                        primary goal. Being able to draw a smile on our clients' faces at promising prices is a true
                        delight for us.
                        <br>
                        For some, being able to get to that important meeting ahead of time is somewhat a priority.
                        Others might find that arriving at a late-night party in a luxurious car can add some edge to
                        their remarkable appearance. The options and features you can get from a Chauffeur service in
                        Dubai are countless. So, why don't you make your call and book your dream car with a driver
                        today
                        <h2>Enjoy Your Trip with Chauffeur Service Dubai</h2>
                        Whether you have the need for speed, divine experience, or valuable service, you can find it in
                        our <b>chauffeur services</b>. You first start by looking at our vast fleet of cars that will
                        have you drooling with amazement. Select the vehicle from your desired category and let us
                        handle the rest from our end. We will assign you a professional driver that will attend to all
                        your transportation needs and keep you punctual and on time. Our drivers know the ins and outs
                        of the city, which makes them the perfect people for this job.
                        <br>
                        Get the ultimate excitement and comfort from driving around Dubai without putting your hands on
                        the wheel. Take control with your mind and not your hands because your chauffeur has you
                        covered. Become the talk on the street and steal every spotlight with your fancy
                        presence.                    </p>
                    </div>
                </div>
            </div>
    </section>
    <section class="mb-3" id="FAQ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="gray fs-25 font-weight-light wow fadeInDown" style="font-weight: bold !important">
                        Frequently Ask Question</h2>
                    <div class="accordion" id="accordionExample">
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="headingOne">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        How can I book my chauffeur service?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    It's effortless and the same as renting a car. All you have to do is contact us by
                                    phone or book on our website. You need to indicate the date of the pick-up and the
                                    time. Make sure you also include how much time you'll be needing the Chauffeur as
                                    they charge per hour.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading2">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse2"
                                            aria-expanded="true" aria-controls="collapse2">
                                        Do I need to call your chauffeur for booking confirmation or pickup location?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse2" class="collapse" aria-labelledby="heading2"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    You don't need to worry about confirming with the driver. Once we receive the
                                    required information from your end plus confirmation, we will personally contact our
                                    chauffeurs and provide them with the details. You'll receive confirmation from us.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading3">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse3"
                                            aria-expanded="true" aria-controls="collapse3">
                                        How can I track my ride?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse3" class="collapse" aria-labelledby="heading3"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Contacting the company's customer hotline will help you track how much time your
                                    driver needs to arrive at your location or if you have any other concerns.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading4">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse4"
                                            aria-expanded="true" aria-controls="collapse4">
                                        If chauffeurs do not appear at the pickup location, what to do?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse4" class="collapse" aria-labelledby="heading4"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    There hasn't been a record of professional chauffeurs not arriving on time to the
                                    location you've requested. However, you could always contact the company's customer
                                    hotline for assistance.
                                    <br>
                                    If anything happened that might affect your scheduled request, we would manage it
                                    and inform you of any rescheduling if needed.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading5">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse5"
                                            aria-expanded="true" aria-controls="collapse5">
                                        How can I cancel my chauffeur's ride, and is it free of charge?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse5" class="collapse" aria-labelledby="heading5"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Canceling your chauffeur ride can be done by simply contacting the company and
                                    informing them 48 hours prior to the scheduled trip. Doing so won't result in any
                                    extra charges, and the client will be fully refunded.
                                    <br>
                                    However, canceling less than 48 hours prior to the scheduled appointment will not be
                                    refunded.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading6">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse6"
                                            aria-expanded="true" aria-controls="collapse6">
                                        How can I extend or shorten my trip time?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse6" class="collapse" aria-labelledby="heading6"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Our trips are usually offered for a maximum of 8 hours at a fixed charge price. Of
                                    course, below that prices will be cheaper. On the other hand, if you require rides
                                    for more than 8 hours, extra charges will be added.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading7">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse7"
                                            aria-expanded="true" aria-controls="collapse7">
                                        Where can I go in my chauffeur-driven car?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse7" class="collapse" aria-labelledby="heading7"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    You can go wherever you wish within the boundaries of the UAE. However, you must
                                    know that some charges may differ according to the location and destination.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading8">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse8"
                                            aria-expanded="true" aria-controls="collapse8">
                                        Can I hire a chauffeur-driven vehicle for parties?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse8" class="collapse" aria-labelledby="heading8"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Enjoying a thrilling adventure and gathering your gang for a party is something
                                    you can hire a chauffeur for. Keep in mind that there's no alcohol allowed
                                    during the trip.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading9">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse9"
                                            aria-expanded="true" aria-controls="collapse9">
                                        What's included in the booking price?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse9" class="collapse" aria-labelledby="heading9"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Booking your very own professional driver will come with complimentary water
                                    inside the vehicle and, of course, a relaxing drive of the century. Other
                                    requirements require extra charges according to the customer's request.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading10">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse" data-target="#collapse10"
                                            aria-expanded="true" aria-controls="collapse10">
                                        What's not included in the booking price?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse10" class="collapse" aria-labelledby="heading10"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Other than the professional driver and complimentary water in the
                                    vehicle, customers must pay extra for any additional requirements.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading11">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse"
                                            data-target="#collapse11"
                                            aria-expanded="true" aria-controls="collapse11">
                                        What mode of payment is accepted?
                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse11" class="collapse" aria-labelledby="heading11"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Our company only accepts full payment in advance through a payment
                                    link provided during the booking process.
                                </div>
                            </div>
                        </div>
                        <div class="card wow fadeInDown">
                            <div class="card-header p-0" id="heading13">
                                <h3 class="mb-0">
                                    <button class="btn  btn-block text-left collapsed font-weight-bold dark-gray"
                                            type="button" data-toggle="collapse"
                                            data-target="#collapse13"
                                            aria-expanded="true" aria-controls="collapse13">
                                        What are your timings?

                                    </button>
                                    <i class="fa fa-plus fs-14 accordion-icon dark-gray"></i>
                                </h3>
                            </div>

                            <div id="collapse13" class="collapse" aria-labelledby="heading13"
                                 data-parent="#accordionExample">
                                <div class="card-body gray">
                                    Our timing schedule is based on your requirements. We will provide
                                    you with a chauffeur at absolutely anytime you need.
                                </div>
                            </div>
                        </div>
                    </div>

    </section>
    <section class="mb-5" id="brands">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="slike">
                        <div class="p-2 p-md-5">
                            <img src="assets/img/1.png" alt="Audi" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/2.png" alt="Rolls Royce" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/3.png" alt="Bentley" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/4.png" alt="Bmw" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/5.png" alt="Bugatti" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/6.png" alt="Chevrolet" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/7.png" alt="Ferrari" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/8.png" alt="Ford" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/9.png" alt="Jaguar" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/10.png" alt="Lamborghini" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/11.png" alt="Land Rover" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/12.png" alt="Maserati" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/13.png" alt="Mclaren" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/14.png" alt="Mercedes" class="w-100">
                        </div>
                        <div class="p-2 p-md-5">
                            <img src="assets/img/15.png" alt="Porsche" class="w-100">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="gradient-bg p-5" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <h3 class="text-white my-3">About Us</h3>
                    <p class="text-white">Providing a relaxing and sensational drive is what we seek to accomplish for
                        our customers.</p>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <h3 class="text-white my-3">Our Services </h3>
                    <p class="text-white"><a rel="nofollow" href="https://luxurycarhire.ae/" style="color:white">Luxury
                            Car Rental</a><br>
                        <a rel="nofollow" href="https://www.vipcarrental.ae/cars/sports-cars/" style="color:white">Sports
                            Car Rental</a><br>
                        <a rel="nofollow" href="https://www.luxurycarrental.ae/range-rover-car-rental-in-dubai"
                           style="color:white">SUV Car Rental</a><br>
                        <a rel="nofollow" href="https://chauffeurservicedubai.ae/#rental" style="color:white">Chauffeur
                            Service</a></p>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <h3 class="text-white my-3">Get In Touch</h3>
                    <div><a rel="nofollow" class="text-white " href="#"><i class="fa fa-map-marker"></i>
                            Next to Holiday Inn Hotel
                            Al Barsha, Sheikh Zayed Road, Dubai ' UAE</a></div>
                    <div><a rel="nofollow" class="text-white " href="tel:+971553451555"><i class="fa fa-phone"></i> +971
                            55 345
                            1555</a></div>
                    <div><a rel="nofollow" class="text-white " href="https://api.whatsapp.com/send?phone=+971553451555"><i
                                    class="fa fa-whatsapp"></i> +971 55 345 1555</a>
                    </div>
                    <div><a rel="nofollow" class="text-white " href="mailto:info@chauffeurservicedubai.ae"><i
                                    class="fa fa-envelope"></i>
                            info@chauffeurservicedubai.ae</a></div>
                </div>
                <div class="col-12 col-sm-6 col-md-3 final">
                    <h3 class="text-white my-3">Location</h3>
                    <div class="w-100">
                       <img src="assets/img/map.png" class="w-100">
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Modal -->
    <div class="modal fade" id="conditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-success accept" data-dismiss="modal">Accept</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content intro-modal">
                <div class="modal-body text-center p-4">
                    <h3 class="yellow">
                        Book Your Car
                    </h3>
                    <form class="text-left" id="bookForm2" data-parsley-validate>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name2">Name <sup>*</sup></label>
                                <input type="text" class="form-control" id="name2"
                                       placeholder="Your Name" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone2">Phone <sup>*</sup></label>
                                <input type="tel" class="form-control phone" id="phone2"
                                       placeholder="Your Number" required=""
                                       data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="car2">Choose your favourite car <sup>*</sup></label>
                            <select id="car2" class="form-control" required="">
                                <option>Rolls Royce Cullinan</option>
                                <option>Bentley Flying Spur</option>
                                <option>Bentley Bentayga</option>
                                <option>Rolls Royce Wraith Black Badge</option>
                                <option>Mercedes S500</option>
                                <option>Rolls Royce Ghost</option>
                                <option>Tesla Model X</option>
                                <option>Cadillac Escalade 2021</option>
                                <option>Audi Q8</option>
                                <option>Mercedes S CLass</option>
                                <option>Porsche Cayenne</option>
                                <option>BMW 740Li</option>
                                <option>Toyota Land Cruiser</option>
                                <option>Cadillac Escalade</option>
                                <option>Lexus LX570</option>
                                <option>Audi Q7</option>
                                <option>GMC Yukon Denali</option>
                                <option>BMW 520i</option>
                                <option>Nissan Patrol</option>
                                <option>Audi A6</option>
                                <option>Range Rover Vogue</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="message2">Your Message</label>
                            <textarea id="message2" class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-warning">Book Now</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </form>
                    <div class="contact-error alert alert-danger ">Try Again Later</div>
                    <div class="contact-success alert alert-success ">Submitted. We will get back to you shortly.</div>
                </div>
            </div>
        </div>
    </div>


    <!--            mobile call-->
    <!-- <div class="mobile-call">
        <a rel="nofollow" href="tel:+971 55 107 3649">
            <i class="fa fa-phone"></i>
        </a>
    </div> -->
    </body>
    <div id="nw-ft-whatsapp2">
        <div style="height: 60px;
    width: 60px;
    border-radius: 50%;
    border: 0 none;
    padding: 11px;
    background: #ffc107;
    margin-left: 17px;margin-top: -10px;">
            <a href="https://api.whatsapp.com/send?phone=+971553451555&text=Hi, I'm contacting you through chauffeurservicedubai.ae. I'd like to inquire about 'Lamborghini Huracan Evo Spider'."><img
                        src="https://www.vipcarrental.ae/wp-content/imgs_2021/icon-whatsapp.svg" alt="" style="height: 38px;
    width: 40px;
    border: 0 none;
    padding: 2px 0 0 0;"></a>
        </div>
    </div>
    <div id="mobile-footer">
        <div id="nw-ft-whatsapp">
            <div style="height: 60px;
    width: 60px;
    border-radius: 50%;
    border: 0 none;
    padding: 11px;
    background: #ffc107;
    margin-left: 17px;margin-top: -10px;">
                <a href="https://api.whatsapp.com/send?phone=+971553451555"><img
                            src="https://www.vipcarrental.ae/wp-content/imgs_2021/icon-whatsapp.svg" alt="" style="height: 38px;
    width: 40px;
    border: 0 none;
    padding: 2px 0 0 0;"></a>
            </div>

            <p>WhatsApp</p>
        </div>
        <div id="nw-ft-chat">

            <p>Live Chat</p>
        </div>
        <div id="nw-ft-call">
            <div id="inner-call">
                <div style="height: 60px;
    width: 60px;
    border-radius: 50%;
    border: 0 none;
    padding: 11px;
    background: #ffc107;
    margin-left: 17px;margin-top: -10px;">
                    <a href="tel:+971553451555"><img
                                src="https://www.vipcarrental.ae/wp-content/imgs_2021/icon-phone.svg" alt="" style="height: 38px;
    width: 40px;
    border: 0 none;
    padding: 4px 0 0 0;"></a>
                </div>

                <p>Call</p>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("nw-ft-chat").onclick = function () {
            Tawk_API.maximize();
            document.getElementById("mobile-footer").classList.add("my-footer-class");
            ;
        };
    </script>
    <style>
        #mobile-footer {
            border-top: 2px solid #ffc107;
            width: 100%;
            display: block !important;
            background: #000000;
            position: fixed;
            bottom: 0;
            min-height: 50px;
            width: 100%;
            text-align: center;
            z-index: 99;
        }

        #mobile-footer p {
            color: #ffc107;
            font-size: 10px;
            line-height: 17px;
        }

        #inner-call {
            position: relative;
            left: -50%;
        }

        #nw-ft-whatsapp {
            float: left;
            width: 100px;
            position: absolute;
            top: -40%;
        }

        #nw-ft-whatsapp2 {
            float: left;
            width: 100px;
            position: fixed;
            bottom: 18px;
            z-index: 999999999999;
        }

        #nw-ft-chat {
            float: right;
            width: 100px;
            position: absolute;
            right: 0;
            top: 65%;
        }

        #nw-ft-call {
            margin: 0 auto;
            width: 100px;
            position: absolute;
            left: 50%;
            top: -40%;
        }

        @media (min-width: 1050px) {
            #mobile-footer {
                display: none !important;
            }
        }

        @media (max-width: 1030px) {
            .whatsapp {
                visibility: hidden;
            }
        }

        @media screen and (min-width: 300px) and (max-width: 320px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-width: 570px) and (max-width: 768px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-width: 790px) and (max-width: 1024px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-width: 370px) and (max-width: 375px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 414px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-width: 490px) and (max-width: 540px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }

        @media screen and (min-height: 600px) and (min-width: 370px) and (max-width: 375px) {
            #mobile-footer {
                min-height: 50px !important;
            }
        }
    </style>
<?php
include_once('include/layout/footer.php');