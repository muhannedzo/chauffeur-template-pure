<?php
$URL = './';


?>

<script src="<?php echo $URL; ?>assets/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/slick.min.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/wow.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/parsley.min.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/intlTelInput.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/header.js" type="text/javascript"></script>
<script src="<?php echo $URL; ?>assets/js/click.js" type="text/javascript"></script>


