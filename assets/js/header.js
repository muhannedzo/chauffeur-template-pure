$(function() {
    $(window).on("scroll", function() {
        if($(window).scrollTop() > 50) {
            $(".mobile-call").addClass("active");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
           $(".mobile-call").removeClass("active");
        }
    });
});