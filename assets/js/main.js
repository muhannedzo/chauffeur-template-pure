$(document).ready(function () {

    var $root = $('html, body');

    $('a[href^="#"]').click(function () {
        $root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top-100
        }, 500);

        return false;
    });
    $(window).scroll(function (){

        if(window.scrollY <= $("#intro").offset().top+110){
            $('.introNav').addClass('active');
            $('.rentalNav').removeClass('active');
            $('.servicesNav').removeClass('active');
            $('.faqNav').removeClass('active');
            $('.contactNav').removeClass('active');
        } if(window.scrollY >= $("#rental").offset().top-110){
            $('.rentalNav').addClass('active');
            $('.introNav').removeClass('active');
            $('.servicesNav').removeClass('active');
            $('.faqNav').removeClass('active');
            $('.contactNav').removeClass('active');

        }if(window.scrollY >= $("#services").offset().top-110){
            $('.servicesNav').addClass('active');
            $('.rentalNav').removeClass('active');
            $('.introNav').removeClass('active');
            $('.faqNav').removeClass('active');
            $('.contactNav').removeClass('active');

        }if(window.scrollY >= $("#FAQ").offset().top-110){
            $('.faqNav').addClass('active');
            $('.servicesNav').removeClass('active');
            $('.rentalNav').removeClass('active');
            $('.introNav').removeClass('active');
            $('.contactNav').removeClass('active');
        }if(window.scrollY >= $("#brands").offset().top-110) {
            $('.contactNav').addClass('active');
            $('.faqNav').removeClass('active');
            $('.servicesNav').removeClass('active');
            $('.rentalNav').removeClass('active');
            $('.introNav').removeClass('active');
        }



    });
    // wow animation
    new WOW().init();
    // end wow animation
    // growShrinkLogo();
    $(".brandSlider").slick({
        useTransform: true,
        centerMode: false,
        infinite: true,
        // response:true,
        arrows:false,
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        // variableWidth: true,
        // padding:'50px',
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    arrows: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    autoplay: true,
                    autoplaySpeed: 2000,
                    arrows: false,
                    slidesToShow: 2
                }
            }
        ]

    });

    // window.onscroll = function() {
    //     growShrinkLogo()
    // };
    //
    // function growShrinkLogo() {
    //
    //     if (document.body.scrollTop > 5 || document.documentElement.scrollTop > 5) {
    //        $('.navbar').addClass('bg-dark-gray');
    //     } else {
    //         $('.navbar').removeClass('bg-dark-gray');
    //     }}
    var input = document.querySelector(".phone");

    $(".phone").focus( function () {
        $(this).val($(".iti__selected-dial-code").text());
    });
    window.intlTelInput(input, {
        nationalMode: true,
        separateDialCode: true,
        preferredCountries: ['ae'],
        utilsScript: "./assets/js/utils.js",
    });
});
$('.accept').click(function () {
   $('.bookModal').removeAttr('disabled')
});
$(document).on('submit', '#bookForm2', function (e) {
    e.preventDefault();
    if ($(this).parsley()) {
        var name=$('#name2').val();
        var phone=$('#phone2').val();
        // var email=$('#email1').val();
        // var from=$('#pick1').val();
        // var to=$('#drop1').val();
        var car=$('#car2').val();
        var message=$('#message2').val();
        $.post("sendMail.php", '&name=' + name + '&phone=' + phone + '&car=' + car+ '&message=' + message, function (result, status, xhr) {
            $('#bookForm2').trigger("reset");
            if (status.toLowerCase() == "error".toLowerCase()) {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);

                });
            } else {
                $('.contact-success').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-success').fadeOut(1000);
                    }, 2000);

                });

            }
        })
            .fail(function () {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);
                    $('#bookForm1').trigger("reset");
                });

            });


    }
});
$(document).on('submit', '#bookForm1', function (e) {
    e.preventDefault();
    if ($(this).parsley()) {
        var name=$('#name1').val();
        var phone=$('#phone1').val();
        var car=$('#car1').val();
        var message=$('#message1').val();
        $.post("sendMail.php", '&name=' + name + '&phone=' + phone + '&car='+car + '&message=' + message, function (result, status, xhr) {
            $('#bookForm1').trigger("reset");
            if (status.toLowerCase() == "error".toLowerCase()) {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);

                });
            } else {
                $('.contact-success').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-success').fadeOut(1000);
                    }, 2000);

                });

            }
        })
            .fail(function () {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);
                    $('#bookForm1').trigger("reset");
                });

            });


    }
});
$(document).on('submit', '#bookForm', function (e) {
    e.preventDefault();
    if ($(this).parsley()) {
        var name=$('#name').val();
        var phone=$('#phone').val();
        // var email=$('#email').val();
        // var from=$('#pick').val();
        // var to=$('#drop').val();
        var car=$('#car').val();
        var message=$('#message').val();
        $.post("sendMail.php", '&name=' + name + '&phone=' + phone + '&car=' +car+ '&message=' + message, function (result, status, xhr) {
            $('#bookForm').trigger("reset");
            if (status.toLowerCase() == "error".toLowerCase()) {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);

                });
            } else {
                $('.contact-success').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-success').fadeOut(1000);
                    }, 2000);

                });

            }
        })
            .fail(function () {
                $('.contact-error').fadeIn(1000, function () {
                    setTimeout(function () {
                        $('.contact-error').fadeOut(1000);
                    }, 2000);
                    $('#bookForm').trigger("reset");
                });

            });


    }
});
$(document).on('click', '.read-more', function () {
    var target=$(this).data('target');
    var type=$('.see-more-'+target).attr('data-type');
    if(type ==='close') {
        $('.see-more-'+target).attr('data-type','open');
        $('.see-more-'+target).removeClass('three-line');
        $('.see-more-'+target).addClass('all-line');
        $(this).text('Read Less');
    }else{
        $('.see-more-'+target).attr('data-type','close');
        $('.see-more-'+target).removeClass('all-line');
        $('.see-more-'+target).addClass('three-line');
        $(this).text('Read More');
    }
});
$(document).on('click','.bookModal',function (e) {
    $('#car2').val($(this).data('car'));
    $('#exampleModal').modal('show');
})
$('.slike').slick({
    dots: false,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 900,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                arrows: false,
                dots: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                speed: 300,
            }
        },  {
            breakpoint: 350,
            settings: {
                arrows: false,
                dots: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                speed: 300,
            }
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
// var currentDate = moment().format("DD/MM/YYYY");
// var min;
// var max=currentDate;
//     $('#pick1').daterangepicker({
//         autoApply: true,
//         singleDatePicker: true,
//         minDate: currentDate,
//         locale: {
//             format: "DD/MM/YYYY",
//         },
//     }, function(start, end) {
//         min=start.format('DD/MM/YYYY');
//         max=end.format('DD/MM/YYYY');
//
//         $(this).val(min);
//         $('#drop1').daterangepicker({
//             "autoApply": true,
//             singleDatePicker: true,
//             minDate:max,
//             locale: {
//                 format: "DD/MM/YYYY",
//             },
//         });
//     });
//   $('#drop1').daterangepicker({
//         "autoApply": true,
//         singleDatePicker: true,
//       minDate:max,
//       locale: {
//           format: "DD/MM/YYYY",
//       },
//     });
// $('#pick').daterangepicker({
//     autoApply: true,
//     singleDatePicker: true,
//     minDate: currentDate,
//     locale: {
//         format: "DD/MM/YYYY",
//     },
// }, function(start, end) {
//     min=start.format('DD/MM/YYYY');
//     max=end.format('DD/MM/YYYY');
//
//     $(this).val(min);
//     $('#drop').daterangepicker({
//         "autoApply": true,
//         singleDatePicker: true,
//         minDate:max,
//         locale: {
//             format: "DD/MM/YYYY",
//         },
//     });
// });
// $('#drop').daterangepicker({
//     "autoApply": true,
//     singleDatePicker: true,
//     minDate:max,
//     locale: {
//         format: "DD/MM/YYYY",
//     },
// });
//

// start loader
// $(window).on('load',function () {
//     $(".louder").fadeOut();
//     $(".preloader").delay(1000).fadeOut("slow");
// });
// end loader